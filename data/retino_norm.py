import csv
import math

FILE = "retinopathy.csv"
NORM_FILE = "retin_norm.csv"
count_of_lines = 1151


def compute_sum_of_values():
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")
        sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,sum13,sum14,sum15,sum16,sum17 = 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0

        for line in reader:
            if len(line) != 20:
                print("Error! Less or more than 20 attributes in line.")
                return
            sum2 += float(line[2])
            sum3 += float(line[3])
            sum4 += float(line[4])
            sum5 += float(line[5])
            sum6 += float(line[6])
            sum7 += float(line[7])
            sum8 += float(line[8])
            sum9 += float(line[9])
            sum10 += float(line[10])
            sum11 += float(line[11])
            sum12 += float(line[12])
            sum13 += float(line[13])
            sum14 += float(line[14])
            sum15 += float(line[15])
            sum16 += float(line[16])
            sum17 += float(line[17])

    return sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,sum13,sum14,sum15,sum16,sum17,count_of_lines


def compute_sum_for_deviation(mean2, mean3, mean4,mean5,mean6,mean7,mean8,mean9,mean10,mean11,mean12,mean13,mean14,mean15,mean16,mean17):
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")

        sum2, sum3, sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,sum13,sum14,sum15,sum16,sum17 = 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        for line in reader:
            if len(line) != 20:
                print("Error! Less than 20 attributes.")
                return
            sum2 += (float(line[2]) - mean2) ** 2
            sum3 += (float(line[3]) - mean3) ** 2
            sum4 += (float(line[4]) - mean4) ** 2
            sum5 += (float(line[5]) - mean5) ** 2
            sum6 += (float(line[6]) - mean6) ** 2
            sum7 += (float(line[7]) - mean7) ** 2
            sum8 += (float(line[8]) - mean8) ** 2
            sum9 += (float(line[9]) - mean9) ** 2
            sum10 += (float(line[10]) - mean10) ** 2
            sum11 += (float(line[11]) - mean11) ** 2
            sum12 += (float(line[12]) - mean12) ** 2
            sum13 += (float(line[13]) - mean13) ** 2
            sum14 += (float(line[14]) - mean14) ** 2
            sum15 += (float(line[15]) - mean15) ** 2
            sum16 += (float(line[16]) - mean16) ** 2
            sum17 += (float(line[17]) - mean17) ** 2

    return math.sqrt(sum2),math.sqrt(sum3),math.sqrt(sum4),math.sqrt(sum5),math.sqrt(sum6),math.sqrt(sum7),\
           math.sqrt(sum8),math.sqrt(sum9),math.sqrt(sum10),math.sqrt(sum11),math.sqrt(sum12),math.sqrt(sum13),\
           math.sqrt(sum14),math.sqrt(sum15),math.sqrt(sum16),math.sqrt(sum17)


def normalize_data(stand_deviations, means):
    f = open(NORM_FILE, "w")
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")

        for line in reader:
            if len(line) != 20:
                print("Error! Less than 20 attributes.")
                return

            for i in range(2,18):
                line[i] = (float(line[i]) - means[i // 2]) / stand_deviations[i // 2]


            newline = ','.join(map(str, line))
            f.write(str(newline) + "\n")
    f.close()


if __name__ == '__main__':
    sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,sum13,sum14,sum15,sum16,sum17,count_of_lines = compute_sum_of_values()

    mean2 = sum2 / count_of_lines
    mean3 = sum3 / count_of_lines
    mean4 = sum4 / count_of_lines
    mean5 = sum5 / count_of_lines
    mean6 = sum6 / count_of_lines
    mean7 = sum7 / count_of_lines
    mean8 = sum8 / count_of_lines
    mean9 = sum9 / count_of_lines
    mean10 = sum10 / count_of_lines
    mean11 = sum11 / count_of_lines
    mean12 = sum12 / count_of_lines
    mean13 = sum13 / count_of_lines
    mean14 = sum14 / count_of_lines
    mean15 = sum15 / count_of_lines
    mean16 = sum16 / count_of_lines
    mean17 = sum17 / count_of_lines

    stand_dev2,stand_dev3,stand_dev4,stand_dev5,stand_dev6,stand_dev7,stand_dev8,stand_dev9,stand_dev10,\
    stand_dev11,stand_dev12,stand_dev13,stand_dev14,stand_dev15,stand_dev16,stand_dev17 = compute_sum_for_deviation(mean2,
        mean3,mean4,mean5,mean6,mean7,mean8,mean9,mean10,mean11,mean12,mean13,mean14,mean15,mean16,mean17)
    normalize_data([stand_dev2,stand_dev3,stand_dev4,stand_dev5,stand_dev6,stand_dev7,stand_dev8,stand_dev9,stand_dev10,\
        stand_dev11,stand_dev12,stand_dev13,stand_dev14,stand_dev15,stand_dev16,stand_dev17],[mean2,mean3,mean4,mean5,mean6,
        mean7,mean8,mean9,mean10,mean11,mean12,mean13,mean14,mean15,mean16,mean17])

