import csv
import math

FILE = "spine_rand.csv"
NORM_FILE = "spine_norm.csv"
count_of_lines = 310


def compute_sum_of_values():
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")
        sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9, sum10, sum11, sum12 = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

        for line in reader:
            if len(line) != 13:
                print("Error! Less or more than 13 attributes in line.")
                return
            sum1 += float(line[0])
            sum2 += float(line[1])
            sum3 += float(line[2])
            sum4 += float(line[3])
            sum5 += float(line[4])
            sum6 += float(line[5])
            sum7 += float(line[6])
            sum8 += float(line[7])
            sum9 += float(line[8])
            sum10 += float(line[9])
            sum11 += float(line[10])
            sum12 += float(line[11])

    return sum1,sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,count_of_lines


def compute_sum_for_deviation(mean1, mean2, mean3, mean4,mean5,mean6,mean7,mean8,mean9,mean10,mean11,mean12):
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")

        sum1, sum2, sum3, sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12= 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        for line in reader:
            if len(line) != 13:
                print("Error! Less than 13 attributes.")
                return
            sum1 += (float(line[0]) - mean1) ** 2
            sum2 += (float(line[1]) - mean2) ** 2
            sum3 += (float(line[2]) - mean3) ** 2
            sum4 += (float(line[3]) - mean4) ** 2
            sum5 += (float(line[4]) - mean5) ** 2
            sum6 += (float(line[5]) - mean6) ** 2
            sum7 += (float(line[6]) - mean7) ** 2
            sum8 += (float(line[7]) - mean8) ** 2
            sum9 += (float(line[8]) - mean9) ** 2
            sum10 += (float(line[9]) - mean10) ** 2
            sum11 += (float(line[10]) - mean11) ** 2
            sum12 += (float(line[11]) - mean12) ** 2

    return math.sqrt(sum1),math.sqrt(sum2),math.sqrt(sum3),math.sqrt(sum4),math.sqrt(sum5),math.sqrt(sum6),math.sqrt(sum7),\
           math.sqrt(sum8),math.sqrt(sum9),math.sqrt(sum10),math.sqrt(sum11),math.sqrt(sum12)


def normalize_data(stand_deviations, means):
    f = open(NORM_FILE, "w")
    with open(FILE) as file:
        reader = csv.reader(file, delimiter=",")

        for line in reader:
            if len(line) != 13:
                print("Error! Less than 13 attributes.")
                return

            for i in range(0,12):
                line[i] = (float(line[i]) - means[i // 2]) / stand_deviations[i // 2]


            newline = ','.join(map(str, line))
            f.write(str(newline) + "\n")
    f.close()


if __name__ == '__main__':
    sum1,sum2,sum3,sum4,sum5,sum6,sum7,sum8,sum9,sum10,sum11,sum12,count_of_lines = compute_sum_of_values()
    mean1 = sum1 / count_of_lines
    mean2 = sum2 / count_of_lines
    mean3 = sum3 / count_of_lines
    mean4 = sum4 / count_of_lines
    mean5 = sum5 / count_of_lines
    mean6 = sum6 / count_of_lines
    mean7 = sum7 / count_of_lines
    mean8 = sum8 / count_of_lines
    mean9 = sum9 / count_of_lines
    mean10 = sum10 / count_of_lines
    mean11 = sum11 / count_of_lines
    mean12 = sum12 / count_of_lines

    stand_dev1,stand_dev2,stand_dev3,stand_dev4,stand_dev5,stand_dev6,stand_dev7,stand_dev8,stand_dev9,stand_dev10,\
    stand_dev11,stand_dev12 = compute_sum_for_deviation(mean1,mean2,mean3,mean4,mean5,mean6,mean7,mean8,mean9,mean10,mean11,mean12)
    normalize_data([stand_dev1,stand_dev2,stand_dev3,stand_dev4,stand_dev5,stand_dev6,stand_dev7,stand_dev8,stand_dev9,stand_dev10,\
    stand_dev11,stand_dev12],[mean1,mean2,mean3,mean4,mean5,mean6,mean7,mean8,mean9,mean10,mean11,mean12])

