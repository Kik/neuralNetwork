# MLP
Multilayer perceptron with backpropagation, gradient descent, MSE (Mean Squared Error), momentum and the minibatch learning.
At present, the evaluation of testing dataset works only for binary classification.

Command line parameters:

    -a,--alfa <arg>           alfa - momentum factor
    -b,--batch <arg>          the size of batch
    -d,--data <arg>           path to learning data
    -i,--iteration <arg>      the number of iteration
    -l,--learningRate <arg>   learning speed
    -t,--testData <arg>       path to testing data
    -x,--input <arg>          the number of neurons in input layer
    -y,--layers <arg>         hidden and output layers

The example of arguments:

    -d data/spine_norm.csv -t data/spine_test.csv -l 0.5 -a 0 -b 10 -i 5000 -x 12 -y 12,8,4,1

Gnuplot draws the plot of movement of MSE. It is an external tool, so you have to install it. You can draw a plot with command where "C://Users/xmiklas1/MLP/plot.dat" is a path to .dat file:

    plot "C://Users/xmiklas1/MLP/plot.dat" using 1:2 with lines