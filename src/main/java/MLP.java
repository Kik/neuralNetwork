import com.opencsv.CSVReader;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author opontes, Miklasova Kristina
 */
public class MLP implements NeuralNetwork {
    private Integer numberOfInputs;
    private List<Layer> layers;

    /**
     * Computes squared error function for one sample.
     *
     * @param results  results computed by network for one sample
     * @param targetValues  target values for input
     * @return the squared subtraction of values
     */
    public Double computeSquaredErrorFunction(List<Double> results, List<Double> targetValues) throws MLPException {
        if (results.size() != targetValues.size()) {
            throw new MLPException("Can not compute error function - the size of result does not equal to the size of target values !");
        }
        Double errorOfSample = 0d;
        for (int i = 0; i < results.size(); i++) {
            errorOfSample += (results.get(i) - targetValues.get(i)) * (results.get(i) - targetValues.get(i));
        }

        return 0.5 * errorOfSample;
    }

    /**
     * Updates weights for all neurons in all layers.
     * @param learningRate the speed of learning
     * @param alfa momentum factor
     */
    public void updateWeights(Double learningRate, Double alfa) {
        for (int i = 0; i < layers.size(); i++) {
            for (int j = 0; j < layers.get(i).getNeurons().size(); j++) {
                for (int k = 0; k < layers.get(i).getNeurons().get(j).getWeights().size(); k++) {
                    Neuron neuron = layers.get(i).getNeurons().get(j);
                    Double weightDelta = - learningRate * neuron.getGradient(k) + alfa * neuron.getPrevDeltaWeight(k);  // momentum factor
                    neuron.setWeight(k, neuron.getWeight(k) + weightDelta);
                    neuron.setPrevDeltaWeight(k, weightDelta);
                }
            }
        }
    }

    /**
     * Computes delta for all neurons.
     * @param targetResults the target values of output neurons
     */
    public void evaluateDelta(List<Double> targetResults) {
        for (int i = layers.size()-1; i >= 0; i--) {
            for (int j = 0; j < layers.get(i).getNeurons().size(); j++) {
                if (i == layers.size() - 1) {   // output layer
                    Double delta = layers.get(i).getResults().get(j) - targetResults.get(j);
                    layers.get(i).setDelta(j, delta);
                } else {     // hidden layers
                    Double delta = 0d;
                    for (int k = 0; k < layers.get(i+1).size(); k++) {  // all neurons from layer above actual layer
                        Double neuronRes = layers.get(i+1).getResults().get(k);
                        delta +=  neuronRes * (1 - neuronRes) * layers.get(i+1).getDelta(k) * layers.get(i+1).getWeight(k, j+1);
                    }
                    layers.get(i).setDelta(j, delta);
                }
            }
        }
    }

    /**
     * Computes gradient for all weights.
     * @param inputs input values
     */
    public void evaluateGradient(List<Double> inputs) {
        for (int i = layers.size()-1; i > 0; i--) {
            for (int j = 0; j < layers.get(i).size(); j++) {
                for (int k = 0; k < layers.get(i).getWeights(j).size(); k++) {
                    Neuron neuron = layers.get(i).getNeurons().get(j);
                    Double neuronRes = layers.get(i).getResults().get(j);
                    Double prevNeuronRes;
                    if (k == 0) {   // bias weight
                        prevNeuronRes = 1d;
                    } else {        // output value from neuron under actual layer
                        prevNeuronRes = layers.get(i-1).getResults().get(k-1);
                    }
                    Double gradChange = neuronRes * (1 - neuronRes) * layers.get(i).getDelta(j) * prevNeuronRes;
                    neuron.setGradient(k, neuron.getGradient(k) + gradChange);
                }
            }
        }

        // first hidden layer (computing with input values)
        for (int j = 0; j < layers.get(0).size(); j++) {
            for (int k = 0; k < layers.get(0).getWeights(j).size(); k++) {
                Neuron neuron = layers.get(0).getNeurons().get(j);
                Double neuronRes = layers.get(0).getResults().get(j);
                Double prevNeuronRes;
                if (k == 0) {
                    prevNeuronRes = 1d;
                } else {
                    prevNeuronRes = inputs.get(j);
                }
                Double gradChange = layers.get(0).getDelta(j) * neuronRes * (1 - neuronRes) * prevNeuronRes;
                neuron.setGradient(k, neuron.getGradient(k) + gradChange);
            }
        }
    }

    /**
     * Sets the delta of all neurons to 0
     */
    public void resetDeltas() {
        for (int i = 0; i < layers.size(); i++) {
            for (int j = 0; j < layers.get(i).size(); j++) {
                layers.get(i).setDelta(j, 0d);
            }
        }
    }

    /**
     * Sets the gradient of all weights of neurons to 0
     */
    public void resetGradient() {
        for (int i = 0; i < layers.size(); i++) {
            for (int j = 0; j < layers.get(i).size(); j++) {
                Neuron neuron = layers.get(i).getNeurons().get(j);
                for (int k = 0; k < neuron.getWeights().size(); k++) {
                    neuron.setGradient(k, 0d);
                }
            }
        }
    }

    @Override
     public List<Double> getResult(List<Double> input) throws MLPException{
        if (input.size() != numberOfInputs) {
            throw new MLPException("Can not compute result! The size of input does not equal to the number of input neurons");
        }

        for (int i = 0; i < layers.size(); i++) {
            input = layers.get(i).evaluate(input);
        }

        return input;
    }

    /**
     * Does forward lerining (getResult) and backpropagation with the updating of weights.
     * @param inputs inputs from dataset
     * @param targetValues target results
     * @param learningRate the speed of learining
     * @param alfa momentum factor
     * @throws MLPException
     */
    public void backpropagation(List<List<Double>> inputs, List<List<Double>> targetValues, Double learningRate, Double alfa) throws MLPException {
        resetDeltas();
        resetGradient();

        for(int i = 0; i < inputs.size(); i++) {   // iterate over all samples
            getResult(inputs.get(i));
            evaluateDelta(targetValues.get(i));   // compute delta for all neurons
            evaluateGradient(inputs.get(i));      // compute gradient for all weights
        }

        updateWeights(learningRate, alfa);
    }

    /**
     * Repeats forward and backpropagation 'numberOfIteration' times. Also saves MSE data for drawing a plot into file 'plot.dat'.
     * @param inputMiniBatch  all input data divided into mini batches
     * @param targetMiniBatch all target values divided into mini batches
     * @param learningRate the speed of learning
     * @param numberOfIteration  the count of iteration
     * @throws MLPException
     */
    public void miniBatchAlgo(List<List<List<Double>>> inputMiniBatch, List<List<List<Double>>> targetMiniBatch,
                              Double learningRate, Double alfa, int numberOfIteration, int totalLengthOfInput) throws MLPException {

        Double globalError = Double.POSITIVE_INFINITY;
        PrintWriter plotWriter = null;

        int counter = 0;
        try {
            plotWriter = new PrintWriter(new FileWriter("plot.dat"));
            plotWriter.println("#\tX\tY");

            while (counter < numberOfIteration) {
                // miniBatch algorithm
                globalError = 0d;
                for (int i = 0; i < inputMiniBatch.size(); i++) {
                    backpropagation(inputMiniBatch.get(i), targetMiniBatch.get(i), learningRate, alfa);

                    for (int k = 0; k < inputMiniBatch.get(i).size(); k++) {
                        List<Double> res = getResult(inputMiniBatch.get(i).get(k));
                        globalError += computeSquaredErrorFunction(res, targetMiniBatch.get(i).get(k));
                    }
                }

                globalError = globalError / totalLengthOfInput;   // MSE computed after each epoch (inputs from all dataset)
                if (counter % 150 == 0) {
                    plotWriter.println("\t" + counter + "\t" + globalError);  // for drawing a plot
                }
                counter++;
            }
        } catch (IOException e) {
            Logger.getLogger(MLP.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            plotWriter.close();
        }

        System.out.println("MSE: " + globalError);
    }

    @Override
    public NeuralNetwork train(File csvFile, Double learningRate, Double alfa, int sizeOfBatch, int numberOfIteration) throws MLPException {
        String[] nextLine;
        List<List<Double>> inputs = new ArrayList<>();
        List<List<Double>> targets = new ArrayList<>();

        if (numberOfIteration < 1) {
            throw new MLPException("Number of iteration must be more than 1!");
        }

        // read all lines from file and divide into inputs & target values
        try {
            CSVReader r = new CSVReader(new FileReader(csvFile));
            while ((nextLine = r.readNext()) != null) {
                List<Double> inputLine = Arrays.stream(nextLine).map(Double::parseDouble).collect(Collectors.toList());
                inputs.add(inputLine.subList(0, numberOfInputs));
                targets.add(inputLine.subList(numberOfInputs, inputLine.size()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int sizeOfMiniBatch = sizeOfBatch;
        if (sizeOfBatch > inputs.size()) {
            sizeOfMiniBatch = inputs.size();
        }

        List<List<List<Double>>> inputMiniBatch = new ArrayList<>();
        List<List<List<Double>>> targetMiniBatch = new ArrayList<>();

        // divide dataset into miniBatches (in the case that (input.size() % sizeofBatch != 0), the last batch have less size)
        int j = 0;
        for (int i = 0; i < inputs.size() / sizeOfMiniBatch; i++) {
            inputMiniBatch.add(inputs.subList(j * sizeOfMiniBatch, j * sizeOfMiniBatch + sizeOfMiniBatch));
            targetMiniBatch.add(targets.subList(j * sizeOfMiniBatch, j * sizeOfMiniBatch + sizeOfMiniBatch));
            j++;
        }

        if (inputs.size() % sizeOfBatch != 0) {  // the rest of inputs (the length of the last minibatch is less than  the lengths of other minibatches)
            inputMiniBatch.add(inputs.subList(j * sizeOfMiniBatch, inputs.size() - 1));
            targetMiniBatch.add(targets.subList(j * sizeOfBatch, targets.size() - 1));
        }

        miniBatchAlgo(inputMiniBatch, targetMiniBatch, learningRate, alfa, numberOfIteration, inputs.size());   // the core of algorithm
        return this;
    }

    @Override
    public NeuralNetwork setLayers(Integer numberOfInput, Integer numberOfOutput, List<Integer> numberOfNeuronsInLayers) {
        this.numberOfInputs = numberOfInput;
        layers = new ArrayList<Layer>();
        for(int i = 0; i < numberOfNeuronsInLayers.size(); i++) {
            if (i == 0) {
                layers.add(new Layer(numberOfNeuronsInLayers.get(0), numberOfInputs, numberOfInputs, numberOfOutput));
            } else {
                layers.add(new Layer(numberOfNeuronsInLayers.get(i), numberOfNeuronsInLayers.get(i-1), numberOfInputs, numberOfOutput));
            }
        }
        return this;
    }
}
