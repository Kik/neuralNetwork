/**
 * @author opontes, Miklasova Kristina
 */
public class MLPException extends Exception {

    public MLPException(String msg) {
        super(msg);
    }
}
