import java.util.ArrayList;
import java.util.List;

/**
 * @author opontes, Miklasova Kristina
 */
public class Layer {
    private List<Neuron> neurons;
    private List<Double> results;
    private List<Double> deltas;

    public Layer(int numberOfNeurons, int prevNumOfNeurons, int inputNeurons, int outputNeurons) {
        neurons = new ArrayList<>();
        results = new ArrayList<>();
        deltas = new ArrayList<>();

        for (int i = 0; i < numberOfNeurons; i++){
            neurons.add(new Neuron(prevNumOfNeurons, inputNeurons, outputNeurons));
            deltas.add(0d);
        }
    }

    /**
     * Evaluates all neurons in a layer and returns results in list.
     * @param inputs values for input neurons
     * @return the list of results for each neuron in a layer
     * @throws MLPException
     */
    public List<Double> evaluate(List<Double> inputs) throws MLPException {
        if (inputs.size() < 1){
            throw new MLPException("No input for evaluation!");
        }

        List<Double> result = new ArrayList<>();

        for (int i = 0; i < neurons.size(); i++) {
            result.add(neurons.get(i).getResult(inputs));
        }

        results = result;

        return results;
    }

    public Integer size() {
        return neurons.size();
    }

    public List<Double> getWeights(int i) {
        return neurons.get(i).getWeights();
    }

    public Double getWeight(int i, int j) {
        return neurons.get(i).getWeight(j);
    }

    public List<Double> getResults() {
        return results;
    }

    public List<Neuron> getNeurons() {
        return neurons;
    }

    public void setDelta(int i, Double delta) {
        deltas.set(i, delta);
    }

    public Double getDelta(int i) {
        return deltas.get(i);
    }
}
