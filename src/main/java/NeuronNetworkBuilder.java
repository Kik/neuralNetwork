import com.opencsv.CSVReader;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author opontes, Miklasova Kristina
 */
public class NeuronNetworkBuilder {

    private static Options getOptions(){
        Options options = new Options();
        options.addOption(Option.builder("d").longOpt( "data" ).required(true).numberOfArgs(1).desc("path to learning data" ).build());
        options.addOption(Option.builder("t").longOpt( "testData" ).required(true).numberOfArgs(1).desc("path to testing data" ).build());
        options.addOption(Option.builder("l").longOpt( "learningRate" ).required(true).numberOfArgs(1).desc("learning speed" ).build());
        options.addOption(Option.builder("a").longOpt( "alfa" ).required(true).numberOfArgs(1).desc("alfa - momentum factor" ).build());
        options.addOption(Option.builder("b").longOpt( "batch" ).required(true).numberOfArgs(1).desc("the size of batch" ).build());
        options.addOption(Option.builder("i").longOpt( "iteration" ).required(true).numberOfArgs(1).desc("the number of iteration" ).build());
        options.addOption(Option.builder("y").longOpt( "layers" ).required(true).hasArgs().valueSeparator(',').desc("hidden and output layers" ).build());
        options.addOption(Option.builder("x").longOpt( "input" ).required(true).numberOfArgs(1).desc("the number of neurons in input layer" ).build());
        return options;
    }

    public static void main(String[] args) throws MLPException {

        // run with arguments like this:
        // -d data/spine_norm.csv -t data/spine_test.csv -l 0.5 -a 0 -b 10 -i 10000 -x 12 -y 12,8,4,1

        String data;
        String testData;
        double learningRate;
        double alfa;
        int batch;
        int iteration;
        int numberOfInputs;
        List<Integer> numberOfNeuronsInLayers = new ArrayList<>();

        new HelpFormatter().printHelp("mlp", getOptions());

        CommandLineParser parser = new DefaultParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse( getOptions(), args );
            data = line.getOptionValue("data");
            testData = line.getOptionValue("testData");
            learningRate = Double.parseDouble(line.getOptionValue("learningRate"));
            alfa = Double.parseDouble(line.getOptionValue("alfa"));
            batch = Integer.parseInt(line.getOptionValue("batch"));
            iteration = Integer.parseInt(line.getOptionValue("iteration"));
            numberOfInputs = Integer.parseInt(line.getOptionValue("input"));
            for (String layer : line.getOptionValues("layers")) {
                numberOfNeuronsInLayers.add(Integer.parseInt(layer));
            }
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
            return;
        }

        if ((alfa < 0) || (alfa >= 1)) {
            alfa = 0d;
        }

        System.out.println("-l " + learningRate + " -a " + alfa + " -b " + batch + " -i " + iteration + " " + numberOfNeuronsInLayers);
        File f = new File(data);
        NeuralNetwork neuralNetwork = new MLP().setLayers(numberOfInputs, numberOfNeuronsInLayers.get(numberOfNeuronsInLayers.size()-1),
                numberOfNeuronsInLayers);

        neuralNetwork.train(f, learningRate, alfa, batch, iteration);

        String[] nextLine;
        List<List<Double>> testInputs = new ArrayList<>();
        List<List<Double>> testTargets = new ArrayList<>();
        int correctResults = 0;

        // read test file and divide into input data and target values
        try {
            CSVReader r = new CSVReader(new FileReader(new File(testData)));
            while ((nextLine = r.readNext()) != null) {
                List<Double> inputLine = Arrays.stream(nextLine).map(Double::parseDouble).collect(Collectors.toList());
                testInputs.add(inputLine.subList(0, numberOfInputs));
                testTargets.add(inputLine.subList(numberOfInputs, inputLine.size()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // evaluate prediction - binary classification
        for (int i = 0; i < testInputs.size(); i++) {
            List<Double> res = neuralNetwork.getResult(testInputs.get(i));
            if (res.size() != 1) {     // because of binary classification - only one output neuron
                throw new IllegalArgumentException();
            }
            if (res.get(0) < 0.5d) {
                res.set(0, 0d);
            } else {
                res.set(0, 1d);
            }
            if (res.equals(testTargets.get(i))) {
                correctResults++;
            }
        }
        System.out.println("Correct results: " + (double)correctResults/(double)testInputs.size()*100 + "%");
    }
}
