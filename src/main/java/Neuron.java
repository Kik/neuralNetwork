import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author opontes, Miklasova Kristina
 */

public class Neuron {

    private List<Double> weights = new ArrayList<>();
    private List<Double> prevDeltaWeights = new ArrayList<>();
    private List<Double> gradients = new ArrayList<>();
    public static final Double BIAS = 1d;

    public Neuron(Integer numOfInputs, int inputNeurons, int outputNeurons) {
        double weightRange = Math.sqrt(6/((double)inputNeurons + (double)outputNeurons));   // Glorot & Bengio weight initialization

        for(int i = 0; i <= numOfInputs; i++) {  // + 1 for bias
            weights.add(-weightRange + (weightRange - (-weightRange)) * new Random().nextDouble());
            gradients.add(0d);
            prevDeltaWeights.add(0d);
        }
    }

    /**
     * Applies logistic sigmiod activation function on the inner potential of neuron.
     * @param inputs inputs for neuron
     * @return result of a neuron
     */
    public Double getResult(List<Double> inputs)
    {
        Double value = 0d;

        value = BIAS * weights.get(0);   // first weight is bias weight
        for(int i = 0; i < inputs.size(); i++) {
            value += weights.get(i+1) * inputs.get(i);
        }

        return 1.0d / (1.0d + Math.exp(-value));   // logistic sigmoid activation function
    }

    public List<Double> getWeights() {
        return weights;
    }

    public double getWeight(int i) {
        return weights.get(i);
    }

    public void setWeight(int i, double weight){
        weights.set(i, weight);
    }

    public void print() {
        System.out.print("|");
    }

    public Double getBias() {
        return BIAS;
    }

    public Double getGradient(int i) {
        return gradients.get(i);
    }

    public void setGradient(int i, Double gradient) {
        gradients.set(i, gradient);
    }

    public Double getPrevDeltaWeight(int i) {
        return prevDeltaWeights.get(i);
    }

    public void setPrevDeltaWeight(int i, Double value) {
        prevDeltaWeights.set(i, value);
    }
}

