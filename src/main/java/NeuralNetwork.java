import java.io.File;
import java.util.List;

/**
 * @author opontes, Miklasova Kristina
 */
public interface NeuralNetwork {

    /**
     * Trains Neural Network with inptu data from csv file
     * @param csvFile file with input data
     * @param learningRate learning rate [0, 1>
     * @param alfa factor for momentum [0, 1]
     * @param sizeOfBatch number of samples in one batch
     * @param numberOfIteration number of epochs
     * @return itself
     * @throws MLPException
     */
    NeuralNetwork train(File csvFile, Double learningRate, Double alfa, int sizeOfBatch, int numberOfIteration) throws MLPException;

    /**
     * Puts data into Neural Network and return some result
     * @param data to put into Neural Network
     * @return result of computation
     */
    List<Double> getResult(List<Double> data) throws MLPException;

    /**
     * Set layers and weights, this method is primary intend for comparison different structures of layers and neurons
     * @param numberOfInputNeurons number of inputs neurons
     * @param numberOfOutputNeurons number of output neurons
     * @param numberOfNeuronsInHiddenLayers number of neurons in each hidden layer, order is from input to output layer
     * @return itself and you can give another command
     */
    NeuralNetwork setLayers(Integer numberOfInputNeurons, Integer numberOfOutputNeurons,
                            List<Integer> numberOfNeuronsInHiddenLayers);

}
